package forum

class User {

    String name
    String lastName
    int age
    String username
    String password

    static constraints = {

        name blank: false, size: 3..50
        lastName blank: false, size: 3..50
        age min: 13
        username blank: false, unique: true 
        password blank: false, matches: "(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z]).{8,}"

    }

	static mapping = {

        tablePerHierarchy true
		
    }
	
	def String toString() {
	
        "($name)"
		
    }
}
