package forum

class Regular extends User{

    int postViews
    int strikesNumber
    int starsNumber
	
    static hasMany = [ post : Post ]

    static constraints = {
 
        postViews min: 0
        strikesNumber range: 0..3
        starsNumber range: 0..5

    }

}
