package forum

class Post {

    String topic
    Date dateCreated
    Date lastUpdate
    boolean itsAllowed
	//List<String> comments
	int rate
  
    static belongsTo = [ forum : Forum, owner : Regular ]
    static hasMany = [ files : File ]
	
    static constraints = {

        topic size: 3..50, blank: false
		lastUpdate validator: { val, obj -> val > obj.dateCreated }
	    rate min: 0
    }

    static mapping = {

        owner column: 'owner_id'
        forum column: 'fatherForum_id'

    }

	/*Por la propiedad autotimestamp de Grails, el framework se encarga de insertar los valores de las variables
	de tipo Date dateCreated y lastUpdate as� como de mantener su consistencia, por lo tanto los m�todos
	siguientes son redundantes pero se dejan expl�citos para prop�sitos del taller.*/
	
    def beforeInsert() {

         dateCreated = new Date()

    }

    def beforeUpdate() {

         lastUpdate = new Date()

    }	
	
	/**def comment(){
		
	}
	
	def rate(){
		
	}
	
	def share(){
		
	}**/

}