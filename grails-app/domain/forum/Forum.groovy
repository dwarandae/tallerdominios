package forum

class Forum {

    String name
    Date dateCreated 
    String category
	
    static belongsTo = [ moderator : Admin ]
    static hasMany = [ post : Post ]
    
    static constraints = {

        name size: 3..20, unique: true, blank: false
        category size: 3..15, blank: false

    }
	
	/*Según requisito del taller, se debe validar que la fecha de creación del foro
	sea mayor o igual a la fecha actual. Grails usa la propiedad autotimestamp para mantener
	la consistencia en BD de dos variables de tipo Date que se llamen dateCreated y lastUpdated,
	por lo tanto por defecto, con el nombre de la variable declarado así es suficiente para cumplir el requisito.
	Se elimina el constraint de la variable dateCreated, pero se deja explícito el evento beforeInsert
	para propósitos del taller.*/

    def beforeInsert() {

	    dateCreated = new Date()

	}
	
	def String toString() {
	
        "($name)"
		
    }

}