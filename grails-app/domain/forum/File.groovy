package forum

class File {

    String fileType
    byte [] content
    double size
    
    static belongsTo = [ post : Post ]

    static constraints = {

        /*El formato MIME se compone de un
	    tipo y un subtipo, el tipo puede contener letras o
	    n�meros, el subtipo puede contener letras, n�meros
	    o los s�mbolos +,- y .
	    Se incluye la restricci�n de m�nimo un caracter
        antes y despu�s de "/" */
        fileType blank: false, matches: "\\w+\\/[\\w+-\\.\\+]+"
        //Tama�o m�ximo, 10MB = 1024*1024*10 Bytes
        content maxSize: 1024*1024*10
        size max: 1024*1024*10 as Double

    }

    static mapping = {
    
        post column: 'post_belongs_id'

    }
	
	/**def download(){
		
	}
	
	def share(){
		
	}**/
}