package forum

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(PostController)
class PostControllerSpec extends Specification {
	
	def post = new Post(topic:"food", dateCreated: new Date(), lastUpdate: new Date(), itsAllowed: true, comments: "hello", rate: 3)

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
    }
	
	void pruebaRate(){
		assert post.rate==4
	}
	
	void pruebaComment(){
		assert post.comment.getAt(0) == "hello"
	}
}
